//
//  RoomDescription.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 24/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation

class RoomDescription : NSObject, NSCoding
{
    var roomName = "Unnamed-Room"
    var roomDescription = [ "Its an empty room" ]
    var objects = [ String: GameObject ]()
    var exits = [ String: LinkDescription ]()
    var images = [ String : LinkDescription ]()
    
    override init() {
        super.init()
        //
    }
    
    required init(coder aDecoder: NSCoder)
    {
        roomName = aDecoder.decodeObjectForKey("roomName") as String
        roomDescription = aDecoder.decodeObjectForKey("roomDescription") as [ String ]
        objects = aDecoder.decodeObjectForKey("objects") as [ String : GameObject ]
        exits = aDecoder.decodeObjectForKey("exits") as [ String : LinkDescription ]
        images = aDecoder.decodeObjectForKey("images") as [ String : LinkDescription ]
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encodeObject(roomName, forKey: "roomName")
        aCoder.encodeObject(roomDescription, forKey: "roomDescription")
        aCoder.encodeObject(objects, forKey: "objects")
        aCoder.encodeObject(exits, forKey: "exits")
        aCoder.encodeObject(images, forKey: "images")
    }
    
    func isCurrentRoom() -> Bool
    {
        if let _ = find(objects.keys, "Player")
        {
            return true
        }
        return false
    }
}
