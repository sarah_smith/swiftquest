//
//  Room.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 16/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation

class Room : NSObject, NSCoding {
    
    /** System name, must be unique; must have no spaces and be URL fragment safe */
    dynamic var roomName = "Unnamed-Room"
    
    /** User visible descriptive name */
    dynamic var visibleName = "Unnamed Room"
    
    override init() {
        //
    }

    required init(coder aDecoder: NSCoder)
    {
        roomName = aDecoder.decodeObjectForKey("roomName") as String
        visibleName = aDecoder.decodeObjectForKey("visibleName") as String
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encodeObject(roomName, forKey: "roomName")
        aCoder.encodeObject(visibleName, forKey: "visibleName")
    }
}