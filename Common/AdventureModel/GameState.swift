//
//  GameState.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 24/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation

class GameState : NSObject, NSCoding
{
    var currentRoom : Room?
    var player = GameObject(name: "Player", descriptiveName: "You")
    
    var rooms = [ Room() ]
    var roomDetails = [ RoomDescription() ]
    
    override init()
    {
        super.init()
        roomDetails[0].objects["Player"] = player
    }
    
    required init(coder aDecoder: NSCoder)
    {
        rooms = aDecoder.decodeObjectForKey("rooms") as [ Room ]
        roomDetails = aDecoder.decodeObjectForKey("roomDetails") as [ RoomDescription ]
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encodeObject(rooms, forKey: "rooms")
        aCoder.encodeObject(roomDetails, forKey: "roomDetails")
    }
    
    func detailsForRoom( roomName: String ) -> RoomDescription?
    {
        for roomDesc in roomDetails
        {
            if roomDesc.roomName == roomName
            {
                return roomDesc
            }
        }
        return nil
    }
    
    func exitsForRoom( roomName: String ) -> [ String: LinkDescription ]
    {
        var exits = [ String: LinkDescription ]()
        if let roomDesc = detailsForRoom(roomName)
        {
            exits = roomDesc.exits
        }
        return exits
    }
}