//
//  SwiftQuestController.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 28/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation
import Cocoa

class SwiftQuestController : NSViewController
{
    /** Persist the currently represented state back to the model */
    func saveCurrentState()
    {
        fatalError("saveCurrentState() not implemented")
    }
    
    /** Load the model to be the currently represented state of the controller */
    func loadCurrentState()
    {
        fatalError("loadCurrentState() not implemented")
    }
}