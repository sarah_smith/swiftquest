//
//  GameStateViewController.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 29/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation
import Cocoa

class GameStateViewController : SwiftQuestController
{
    var gameState : GameState = GameState()
    
    override func viewDidLoad()
    {
        loadCurrentState()
    }
    
    override func viewWillDisappear() {
        saveCurrentState()
    }
    
    override func viewDidLayout() {
//        <#code#>
    }

    override func loadCurrentState() {
        let sdc = NSDocumentController.sharedDocumentController() as NSDocumentController
        if let doc = sdc.currentDocument as? Document
        {
            if let gs = doc.gameState
            {
                gameState = gs
            }
        }
    }
    
    override func saveCurrentState()
    {
        let sdc = NSDocumentController.sharedDocumentController() as NSDocumentController
        let doc = sdc.currentDocument as Document
        doc.gameState = gameState
    }
}