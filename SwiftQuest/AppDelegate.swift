//
//  AppDelegate.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 15/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

