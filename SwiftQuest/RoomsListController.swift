//
//  RoomsListController.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 26/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation
import Cocoa

class RoomsListController : SwiftQuestController, NSTableViewDelegate
{
    let nameFormatter = FragmentValueFormatter()
    
    @IBOutlet var tableView : NSTableView!
    @IBOutlet var roomArrayController : NSArrayController!
    
    var rooms : [Room] {
        willSet {
            for rm in rooms
            {
                stopObserving(rm)
            }
        }
        didSet {
            for rm in rooms
            {
                startObserving(rm)
            }
        }
    }
    
    override init?(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        rooms = [ Room ]()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required override init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
// MARK: SwiftQuestController compliance
    
    override func saveCurrentState()
    {
        let pc = parentViewController as GameStateViewController
        pc.gameState.rooms = rooms
    }
    
    override func loadCurrentState()
    {
        let pc = parentViewController as GameStateViewController
        rooms = pc.gameState.rooms
    }
    
// MARK: Event handlers
    
    deinit {
        NSLog("RoomsListController \(self) deinit - rooms \(rooms.count)")
    }
    
    override func viewDidLoad()
    {
        //
    }
    
    override func viewWillDisappear() {
        NSLog("RoomsListController \(self) viewWillDisappear - rooms \(rooms.count)")
        for rm in rooms
        {
            stopObserving(rm)
        }
    }
    
    @IBAction func runGame(sender: AnyObject)
    {
        let w = tableView.window!
        let editingEnded = w.makeFirstResponder(w)
        if !editingEnded
        {
            NSLog("Unable to end editing!")
            return
        }
        let documentController = NSDocumentController.sharedDocumentController() as NSDocumentController
        let document = documentController.currentDocument as Document
        
    }
    
    @IBAction func addRoom(sender: AnyObject)
    {
        let w = tableView.window!
        let editingEnded = w.makeFirstResponder(w)
        if !editingEnded
        {
            NSLog("Unable to end editing!")
            return
        }
        let undo = undoManager!
        if undo.groupingLevel > 0
        {
            undo.endUndoGrouping()
            undo.beginUndoGrouping()
        }
        let rm : Room = roomArrayController.newObject() as Room
        roomArrayController.addObject(rm)
        roomArrayController.rearrangeObjects()
        let roomsListSorted = roomArrayController.arrangedObjects as Array<Room>
        if let ix = find(roomsListSorted, rm)
        {
            NSLog("Starting edit on row \(ix)")
            tableView.editColumn(0, row: ix, withEvent: nil, select: true)
        }
    }
    
    @IBAction func editRoomDetails(sender: AnyObject)
    {
        NSLog("Editing room: \(sender)")
    }
    
    /** Key-value collections object function - called to remove an object from the "rooms" collection */
    func insertObject(room: Room, inRoomsAtIndex index: Int)
    {
        NSLog("Adding \(room) to \(rooms)")
        
        // Add the inverse of the insert to the undo stack
        let undo = self.undoManager!
        undo.prepareWithInvocationTarget(self).removeObjectFromRoomsAtIndex(index)
        if (!undo.undoing)
        {
            undo.setActionName("Add Room")
        }
        
        // Perform the action insert
        startObserving(room)
        rooms.insert(room, atIndex: index)
    }
    
    /** Key-value collections object function - called to remove an object from the "rooms" collection */
    func removeObjectFromRoomsAtIndex(index: Int)
    {
        let room = rooms[index]
        NSLog("Removing room \(room)")
        
        // Add the inverse of the remove to the undo stack
        let undo = self.undoManager!
        undo.prepareWithInvocationTarget(self).insertObject(room, inRoomsAtIndex: index)
        if (!undo.undoing)
        {
            undo.setActionName("Remove Room")
        }
        
        // Perform the actual remove
        stopObserving(room)
        rooms.removeAtIndex(index)
    }
    
// MARK: Key-value observing of Room
    
    func startObserving(room: Room)
    {
//        NSLog("%@ Start observing: %@", self, room)
        room.addObserver(self, forKeyPath: "roomName", options: NSKeyValueObservingOptions.Old, context: &DocumentObservingCtx)
        room.addObserver(self, forKeyPath: "visibleName", options: NSKeyValueObservingOptions.Old, context: &DocumentObservingCtx)
    }
    
    func stopObserving(room: Room)
    {
//        NSLog("%@ Stop observing: %@", self, room)
        room.removeObserver(self, forKeyPath: "roomName", context: &DocumentObservingCtx)
        room.removeObserver(self, forKeyPath: "visibleName", context: &DocumentObservingCtx)
    }
    
    func change(keyPath : String, ofObject object: NSObject, toValue: AnyObject?)
    {
        NSLog("Changed object: \(object) to be \(toValue)")
        object.setValue(toValue, forKeyPath: keyPath)
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>)
    {
        if (context != &DocumentObservingCtx)
        {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
            return
        }
        var oldValue: NSObject? = change[NSKeyValueChangeOldKey] as? NSObject
        if (oldValue == NSNull())
        {
            NSLog("oldValue is nil")
            oldValue = nil
        }
        
        let undo = self.undoManager!
        undo.prepareWithInvocationTarget(self).change(keyPath, ofObject: object as NSObject, toValue: oldValue)
        undo.setActionName("Edit")
    }
    
}