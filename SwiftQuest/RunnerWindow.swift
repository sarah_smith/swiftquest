//
//  RunnerWindow.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 23/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Foundation
import AppKit
import Cocoa

class RunnerWindow : NSWindowController
{
    let questView : QuestView! = QuestView(nibName:"QuestView", bundle: nil)
    
    override func windowDidLoad()
    {
        let w = window!
        w.contentView = questView.view
        
        let screen : NSScreen! = NSScreen.mainScreen()
        let mainScreenFrame = screen.frame
        
        w.setFrame(mainScreenFrame, display: true)
    }
    
}