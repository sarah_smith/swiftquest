//
//  Document.swift
//  SwiftQuest
//
//  Created by Sarah Smith on 15/12/2014.
//  Copyright (c) 2014 Sarah Smith. All rights reserved.
//

import Cocoa

var DocumentObservingCtx = 0

class Document: NSDocument
{
    var gameState : GameState?
    
    @IBOutlet weak var mainView: NSView!
    
    @IBOutlet weak var rootViewController : GameStateViewController!

    var currentViewController : SwiftQuestController?
    var runnerWindow : RunnerWindow?
    
    var viewControllers = [ String: SwiftQuestController ]()
    
    var viewNameForDocument : String {
        get {
            if let fu = fileURL
            {
                NSLog("viewNameForDocument - fileURL: \(fileURL)")
                if let p = fu.path
                {
                    NSLog("viewNameForDocument - fileURL.path: \(p)")
                    let def = NSUserDefaults.standardUserDefaults()
                    if let sessionDict = def.dictionaryForKey(p)
                    {
                        NSLog("    got session dict: \(sessionDict)")
                        if let viewName = sessionDict["viewName"] as? String
                        {
                            return viewName
                        }
                    }
                }
            }
            return "RoomsList"
        }
        set(viewName) {
            if let fu = fileURL
            {
                if let p = fu.path
                {
                    var updatedDict = Dictionary<NSObject, AnyObject>()
                    let def = NSUserDefaults.standardUserDefaults()
                    if let sessionDict = def.dictionaryForKey(p)
                    {
                        updatedDict = def.dictionaryForKey(p)!
                    }
                    updatedDict.updateValue(viewName, forKey: "viewName")
                    def.setObject(updatedDict, forKey: p)
                    NSLog("Set for key: \(p) dict obj: \(updatedDict)")
                }
            }
        }
    }
    
    func showAlert( message: String, title: String )
    {
        var alert = NSAlert()
        alert.informativeText = message
        alert.messageText = title
        alert.addButtonWithTitle("OK")
        alert.runModal()
    }
    
    override func windowControllerDidLoadNib(aController: NSWindowController)
    {
        super.windowControllerDidLoadNib(aController)
        // Add any code here that needs to be executed once the windowController has loaded the document's window.

        NSLog("windowControllerDidLoadNib")
        switchToView(viewNameForDocument)
    }
    
    func switchToView( viewName: String )
    {
        NSLog("switchToView: \(viewName)")
        let prevViewController = currentViewController
        if let vcExisting = viewControllers[viewName]
        {
            currentViewController = vcExisting
            vcExisting.saveCurrentState()
        }
        else
        {
            switch viewName {
                case "RoomsList":
                    currentViewController = RoomsListController(nibName: "RoomsList", bundle: nil)
                case "RoomDetails":
                    currentViewController = RoomDetailsController(nibName: "RoomDetails", bundle: nil)
                default:
                    fatalError("Bad view switch request: \(viewName)")
            }
            viewControllers[viewName] = currentViewController
            rootViewController.addChildViewController(currentViewController!)
        }
        let vc = currentViewController!
        vc.view.frame = mainView.frame
        vc.view.autoresizingMask = NSAutoresizingMaskOptions.ViewMaxXMargin | NSAutoresizingMaskOptions.ViewMaxYMargin
        if let fromViewController = prevViewController
        {
            rootViewController.transitionFromViewController(fromViewController, toViewController: vc, options: NSViewControllerTransitionOptions.SlideForward, completionHandler: nil)
        }
        else
        {
            mainView.addSubview(vc.view)
        }
        vc.loadCurrentState()
        viewNameForDocument = viewName
    }
    
    override class func autosavesInPlace() -> Bool {
        return true
    }

    override var windowNibName: String? {
        // Returns the nib file name of the document
        // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this property and override -makeWindowControllers instead.
        return "Document"
    }

    override func dataOfType(typeName: String, error outError: NSErrorPointer) -> NSData?
    {
        let result = NSKeyedArchiver.archivedDataWithRootObject(rootViewController.gameState)
        return result
    }

    override func readFromData(data: NSData, ofType typeName: String, error outError: NSErrorPointer) -> Bool
    {
        NSLog("readFromData: \(typeName)")
        let result = NSKeyedUnarchiver.unarchiveObjectWithData(data) as GameState
        gameState = result
        if let rvc = rootViewController
        {
            rvc.loadCurrentState()
        }
        return true
    }
    
    override func saveDocument(sender: AnyObject?) {
        super.saveDocument(sender)
        if let vc = currentViewController
        {
            viewNameForDocument = vc.nibName!
        }
    }
    
    override func saveDocumentAs(sender: AnyObject?) {
        super.saveDocumentAs(sender)
        if let vc = currentViewController
        {
            viewNameForDocument = vc.nibName!
        }
    }
}
